import dash_auth
from dash import dcc, dash_table, Dash, html
from dash.dependencies import Input, Output, State
import plotly
import plotly.express as px
import pandas as pd

# setup parameters
external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
VALID_USERNAME_PASSWORD_PAIRS = {"hello": "world"}


app = Dash(__name__, title="budgetMinMax", external_stylesheets=external_stylesheets)
auth = dash_auth.BasicAuth(app, VALID_USERNAME_PASSWORD_PAIRS)

params = ["Expense", "Amount"]
app.layout = html.Div(
    [
        html.H1("budgetMinMax"),
        dcc.Input(
            id="input_monthly_income",
            type="number",
            placeholder="input monthly income",
        ),
        dash_table.DataTable(
            id="table-editing-simple",
            columns=([{"id": p, "name": p} for p in params]),
            data=[dict(Model=i, **{param: 0 for param in params}) for i in range(1, 3)],
            editable=True,
            row_deletable=True,
        ),
        html.Button("Add Row", id="adding-rows-button", n_clicks=0),
        html.Div(id="out-all-types"),
        dcc.Graph(id="pie-chart-output"),
    ]
)


@app.callback(
    Output("out-all-types", "children"),
    Input("input_monthly_income", "value"),
)
def cb_render(*vals):
    return " | ".join((str(val) for val in vals if val))


@app.callback(
    Output("table-editing-simple", "data"),
    Input("adding-rows-button", "n_clicks"),
    State("table-editing-simple", "data"),
    State("table-editing-simple", "columns"),
)
def add_row(n_clicks, rows, columns):
    if n_clicks > 0:
        rows.append({c["id"]: "" for c in columns})
    return rows


@app.callback(
    Output("pie-chart-output", "figure"),
    Input("table-editing-simple", "data"),
    Input("table-editing-simple", "columns"),
)
def piechart_output(rows, columns):
    df = pd.DataFrame(rows, columns=[c["name"] for c in columns])
    fig = px.pie(data_frame=df, names="Expense", values="Amount", title="a budget")
    return fig


if __name__ == "__main__":
    app.run_server(debug=True)
